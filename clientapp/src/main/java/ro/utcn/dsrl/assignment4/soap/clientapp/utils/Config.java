package ro.utcn.dsrl.assignment4.soap.clientapp.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class Config {
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this is the package name specified in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("ro.utcn.dsrl.assignment4.soap.clientapp.generated");
        return marshaller;
    }

    @Bean
    public ClientCommands clientCommands(Jaxb2Marshaller marshaller) {
        ClientCommands clientCommands = new ClientCommands();
        clientCommands.setDefaultUri("http://http://localhost:8080/ws/users");
        clientCommands.setMarshaller(marshaller);
        clientCommands.setUnmarshaller(marshaller);
        return clientCommands;
    }
}
