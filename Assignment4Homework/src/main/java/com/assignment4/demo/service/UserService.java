package com.assignment4.demo.service;

import com.assignment4.demo.exception.ResourceNotFoundException;
import com.assignment4.demo.model.User;
import com.assignment4.demo.repository.UserRepository;
import com.assignment4.demo.util.Converter;
import com.in28minutes.users.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    Converter converter = new Converter();

    public List<UserDetails> getAllUsers(){
        List<User> users = userRepository.findAll();
        List<UserDetails> userDTOS = new ArrayList<>();
        for (User user : users){
            userDTOS.add(converter.objectToDTO(user));
        }
        return userDTOS;
    }

    public UserDetails createUser(UserDetails userDTO){
        User user = converter.DTOToObject(userDTO);
        user = userRepository.save(user);
        return converter.objectToDTO(user);
    }

    public UserDetails getUserById(Integer userId){
        System.out.println("getUserById - service " + userId);
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        return converter.objectToDTO(user);
    }

    public UserDetails updateUser(Integer userId, UserDetails newUserDTO){

        User user  = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        if (user != null){
            user = converter.DTOToObject(newUserDTO);
            user.setUserId(userId);
            user = userRepository.save(user);
        }
        return converter.objectToDTO(user);

    }

    public ResponseEntity<?> deleteUser(Integer userId){
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        userRepository.delete(user);
        return ResponseEntity.ok().build();
    }


}
