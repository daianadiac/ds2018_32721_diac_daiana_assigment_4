package com.assignment4.demo.util;

import com.assignment4.demo.model.User;
import com.in28minutes.users.UserDetails;

public class Converter {

    public Converter(){

    }

    public UserDetails objectToDTO(User user){
        UserDetails userDetails = new UserDetails();
        userDetails.setId(user.getUserId());
        userDetails.setUsername(user.getUserName());
        userDetails.setPassword(user.getPassword());
        return userDetails;
    }

    public User DTOToObject(UserDetails userDetails){
        User user = new User();
        user.setUserId(userDetails.getId());
        user.setUserName(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());
        return user;
    }
}
