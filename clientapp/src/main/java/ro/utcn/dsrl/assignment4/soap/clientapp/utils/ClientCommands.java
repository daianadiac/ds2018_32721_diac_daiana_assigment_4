package ro.utcn.dsrl.assignment4.soap.clientapp.utils;

import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.slf4j.Logger;
import ro.utcn.dsrl.assignment4.soap.clientapp.generated.*;
import java.util.List;

public class ClientCommands extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(ClientCommands.class);

    public List<UserDetails> getAllUsers(){
        GetAllUsersResponse response = (GetAllUsersResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws/users", new GetAllUsersRequest());
        List<UserDetails> users = response.getUserDetails();
        return users;
    }

    public UserDetails getUserById(int id){
        GetUserDetailsRequest getUserDetailsRequest = new GetUserDetailsRequest();
        getUserDetailsRequest.setId(id);
        log.info("Looking for user with id: "+id);
        GetUserDetailsResponse getUserDetailsResponse = (GetUserDetailsResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws/users", getUserDetailsRequest);
        if (getUserDetailsResponse.getUserDetails() == null){
            return null;
        } else {
            return getUserDetailsResponse.getUserDetails();
        }
    }

    public UserDetails createUser(String userName, String password){
        AddUserRequest request = new AddUserRequest();
        request.setUsername(userName);
        request.setPassword(password);
        AddUserResponse response = (AddUserResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws/users", request);
        return response.getUserDetails();
    }

    public UserDetails updateUser(int id, String userName, String password){
        UpdateUserRequest request = new UpdateUserRequest();
        UserDetails userDetails = new UserDetails();
        userDetails.setId(id);
        userDetails.setUsername(userName);
        userDetails.setPassword(password);
        request.setUserDetails(userDetails);
        UpdateUserResponse response = (UpdateUserResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws/users", request);
        return response.getUserDetails();
    }

    public void deleteUser(int id){
        DeleteUserRequest request = new DeleteUserRequest();
        request.setId(id);
        getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws/users", request);
    }

}
