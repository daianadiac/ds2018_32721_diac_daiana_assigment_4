package com.assignment4.demo.endpoint;

import com.assignment4.demo.service.UserService;
import com.in28minutes.users.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.bind.JAXBElement;
import java.util.List;

@Endpoint
public class UserDetailsEndpoint {

    private static final String NAMESPACE_URL = "http://in28minutes.com/users";

    @Autowired
    UserService userService;


    public UserDetailsEndpoint(){

    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "GetUserDetailsRequest")
    @ResponsePayload
    public GetUserDetailsResponse GetUserDetailsById(@RequestPayload GetUserDetailsRequest userDetailsRequest) {
        GetUserDetailsResponse userDetailsResponse = new GetUserDetailsResponse();

        UserDetails userDetails = userService.getUserById(userDetailsRequest.getId());

        userDetailsResponse.setUserDetails(userDetails);

        return userDetailsResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "GetAllUsersRequest")
    @ResponsePayload
    public GetAllUsersResponse GetAllUsers() {
        GetAllUsersResponse getAllUsersResponse = new GetAllUsersResponse();

        List<UserDetails> users = userService.getAllUsers();

        getAllUsersResponse.setUserDetails(users);

        return getAllUsersResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "AddUserRequest")
    @ResponsePayload
    public AddUserResponse addNewUser(@RequestPayload AddUserRequest addUserRequest){
        AddUserResponse addUserResponse = new AddUserResponse();

        UserDetails newUser = new UserDetails();
        newUser.setUsername(addUserRequest.getUsername());
        newUser.setPassword(addUserRequest.getPassword());
        UserDetails userDetails = userService.createUser(newUser);
        addUserResponse.setUserDetails(userDetails);
        return addUserResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "UpdateUserRequest")
    @ResponsePayload
    public UpdateUserResponse updateUser(@RequestPayload UpdateUserRequest updateUserRequest){
        UpdateUserResponse updateUserResponse = new UpdateUserResponse();

        UserDetails userDetails = userService.updateUser(updateUserRequest.getUserDetails().getId(), updateUserRequest.getUserDetails());
        updateUserRequest.setUserDetails(userDetails);
        return updateUserResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "DeleteUserRequest")
    @ResponsePayload
    public DeleteUserResponse deleteUser(@RequestPayload DeleteUserRequest deleteUserRequest){
        DeleteUserResponse deleteUserResponse = new DeleteUserResponse();
        userService.deleteUser(deleteUserRequest.getId());

        return deleteUserResponse;
    }



}
