package ro.utcn.dsrl.assignment4.soap.clientapp.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.event.ActionEvent;

import java.awt.EventQueue;
import java.util.List;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import ro.utcn.dsrl.assignment4.soap.clientapp.generated.UserDetails;

@SpringBootApplication
public class ClientappApplication extends JFrame {

    @Autowired
    ClientCommands clientCommands;
    private JPanel contentPane;
    private JLabel lblUserId;
    private JLabel lblUserName;
    private JLabel lblUserPassword;
    private JTextField userName;
    private JTextField userPassword;
    private JTextField idUser;
    private JButton btnFindAllUsers;
    private JButton btnCreateUser;
    private JButton btnUpdateUser;
    private JButton btnDeleteUser;
    private JButton btnFindUserById;
    private JTextArea textArea;

    public ClientappApplication() {
        setTitle("Client View");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(50, 50, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        //find all packages
        btnFindAllUsers = new JButton("ALL USERS");
        btnFindAllUsers.setBounds(10, 250, 120, 20);
        contentPane.add(btnFindAllUsers);
        btnFindUserById = new JButton("FIND USER");
        btnFindUserById.setBounds(10, 230, 120, 20);
        contentPane.add(btnFindUserById);
        btnFindUserById.setVisible(true);
        lblUserId = new JLabel("ID:");
        lblUserId.setBounds(10, 50, 120, 20);
        contentPane.add(lblUserId);
        idUser = new JTextField();
        idUser.setBounds(10, 70, 120, 20);
        contentPane.add(idUser);
        idUser.setColumns(10);
        lblUserName = new JLabel("USERNAME:");
        lblUserName.setBounds(10, 90, 120, 20);
        contentPane.add(lblUserName);
        userName = new JTextField();
        userName.setBounds(10, 110, 120, 20);
        contentPane.add(userName);
        userName.setColumns(10);
        lblUserPassword = new JLabel("PASSWORD:");
        lblUserPassword.setBounds(10, 130, 120, 20);
        contentPane.add(lblUserPassword);
        userPassword = new JTextField();
        userPassword.setBounds(10, 150, 120, 20);
        contentPane.add(userPassword);
        userPassword.setColumns(10);
        btnCreateUser = new JButton("CREATE USER");
        btnCreateUser.setBounds(10, 170, 120, 20);
        contentPane.add(btnCreateUser);
        btnUpdateUser = new JButton("UPDATE USER");
        btnUpdateUser.setBounds(10, 190, 120, 20);
        contentPane.add(btnUpdateUser);
        btnDeleteUser = new JButton("DELETE USER");
        btnDeleteUser.setBounds(10, 210, 120, 20);
        contentPane.add(btnDeleteUser);


        //group of text fields
        JTextField[] textFields =  new JTextField[]{idUser, userName, userPassword};

        //display results
        textArea = new JTextArea();
        textArea.setBounds(150, 70, 400, 200);
        contentPane.add(textArea);


        //GetAllUsersListener
        btnFindUserById.addActionListener((ActionEvent event) -> {
            if (idUser.getText() != null) {
                int id = Integer.parseInt(idUser.getText());
                UserDetails userDetails = clientCommands.getUserById(id);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Id: " + userDetails.getId() + " Username: " + userDetails.getUsername() + " Password: " + userDetails.getPassword() + "\n");
                print(stringBuilder.toString());
                clear(textFields);
            }
        });

        btnCreateUser.addActionListener((ActionEvent event) -> {
            if (userName.getText() != null && userPassword.getText() != null) {
                UserDetails userDetails = clientCommands.createUser(userName.getText(), userPassword.getText());
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Id: " + userDetails.getId() + " Username: " + userDetails.getUsername() + " Password: " + userDetails.getPassword() + "\n");
                print(stringBuilder.toString());
                clear(textFields);
            } else {
                print("Insert ALL data!");
            }
        });

        btnUpdateUser.addActionListener((ActionEvent event) -> {
            if (idUser.getText() != null && userName.getText() != null && userPassword.getText() != null) {
                int id = Integer.parseInt(idUser.getText());
                UserDetails userDetails = clientCommands.updateUser(id, userName.getText(), userPassword.getText());
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Id: " + userDetails.getId() + " Username: " + userDetails.getUsername() + " Password: " + userDetails.getPassword() + "\n");
                print(stringBuilder.toString());
                clear(textFields);
            } else {
                print("Insert ALL data!");
            }
        });

        btnDeleteUser.addActionListener((ActionEvent event) -> {
            if (idUser.getText() != null) {
                int id = Integer.parseInt(idUser.getText());
                clientCommands.deleteUser(id);
                print("User has been deleted!");
                clear(textFields);
                textArea.setText("");
            } else {
                print("Insert ALL data!");
            }
        });

        btnFindAllUsers.addActionListener((ActionEvent event) -> {
            List<UserDetails> users = clientCommands.getAllUsers();
            if (users == null || users.size() == 0){
                print("There are no users:(");
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                for (UserDetails userDetails : users){
                    stringBuilder.append("Id: " + userDetails.getId() + " Username: " + userDetails.getUsername() + " Password: " + userDetails.getPassword() + "\n");
                }
                print(stringBuilder.toString());
            }
        });


    }

    public void print(String message) {
        textArea.setText(message);
    }

    public void clear(JTextField[] textFields) {
        for (JTextField text : textFields){
            text.setText("");
        }
    }



    public static void main(String[] args) {

        ConfigurableApplicationContext ctx = new SpringApplicationBuilder(ClientappApplication.class)
                .headless(false).run(args);

        EventQueue.invokeLater(() -> {
            ClientappApplication ex = ctx.getBean(ClientappApplication.class);
            ex.setVisible(true);
        });
    }
}

