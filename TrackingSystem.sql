create database TrackingSystem;
use TrackingSystem;
create table Role(
Id int not null auto_increment primary key,
Name nvarchar(20) not null
);
create table User(
Id int not null auto_increment primary key,
UserName nvarchar(50) not null unique,
Password nvarchar(50) not null
);
create table RoleUser(
IdUser int not null,
IdRole int not null,
CONSTRAINT FK_User_Role FOREIGN KEY (IdUser) REFERENCES User(Id),
CONSTRAINT FK_Role_User FOREIGN KEY (IdRole) REFERENCES Role(Id)
);

create table Package(
Id int not null auto_increment primary key,
Name nvarchar(20) not null,
Description nvarchar(128),
Sender int not null,
Receiver int not null,
SenderCity nvarchar(50) not null,
ReceiverCity nvarchar(50) not null,
Tracking bit not null default 0  ,
CONSTRAINT FK_User_PackageS FOREIGN KEY (Sender) REFERENCES User(Id),
CONSTRAINT FK_User_PackageR FOREIGN KEY (Receiver) REFERENCES User(Id)
);

create table Tracking(
Id int not null auto_increment primary key,
City nvarchar(50) not null,
Time datetime not null,
IdPackage int not null,
CONSTRAINT FK_Tracking_Package FOREIGN KEY (IdPackage) REFERENCES Package(Id)
);

insert into Role(Name) values ('User');
insert into Role(Name) values ('Admin');
insert into User(UserName,Password) values('Admin','123456');
insert into RoleUser values(1,2);
